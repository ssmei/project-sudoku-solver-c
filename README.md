Project for solving SUDOKU using C language
To build project type "make" / "make all" in command prompt.

To clean the current build type "make cleanU" in command prompt for Unix`s version of system.
To clean the current build type "make cleanW" in command prompt for Windows' version of system.

! You need to have gcc to build.
After building, executable is created in "release" folder named solver.exe. Please run the executable from it's folder "release".

To choose sudoku you would like to solve, visit folder "release/input".
	There fill a file "fillMe.txt" in format described in "specfun.docx".
	There you also have some included sudoku.

After executing "release/solver.exe" solved sudoku will be inserted to file "release/output/solved.txt".

Try to use different brute-force method to compare time of execution for each method for specific sudoku (I recommend preinstalled sudokuAgainstBruteForce.txt).

For more information read "specfun.pdf" and "spectech.pdf". Each file in folder src/ and inc/ has comments in English.


Author:
Serafym Smei
serafym.smei@gmail.com