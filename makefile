CC=gcc
CFLAGS=-Wall
OUTDIR = release
OUTNAME = solver.exe
OUT = $(OUTDIR)/$(OUTNAME)
INC = -Iinc
ODIR = obj
SDIR = src
_OBJS = main.o dataHandle.o gui.o helpTable.o bruteForce.o
OBJS = $(patsubst %,$(ODIR)/%,$(_OBJS))

all: $(OUT)

$(OUT): $(OBJS)
	$(CC)  -o $(OUT) $^ $(CFLAGS)

$(ODIR)/%.o: $(SDIR)/%.c
	$(CC) -c $(INC) -o $@ $< $(CFLAGS)


cleanU:
	-rm -f $(ODIR)/*.o $(OUT)
	
cleanW:
	DEL /F /Q ".\$(ODIR)\*.o" ".\$(OUTDIR)\$(OUTNAME)"