#include "definer.h"

void bruteCreate(cellOptions helpTable[SIZE][SIZE], int x, int y);

void helpTableComplete(cellOptions helpTable[SIZE][SIZE]);

// Calculates if "insert" can be put in mainTable[x][y]
flag insertMistake(cell mainTable[SIZE][SIZE], int x, int y, int insert);

bruteState bruteForce(cell mainTable[SIZE][SIZE],
					  cellOptions helpTable[SIZE][SIZE],
					  int x,
					  int y);

int bruteTableCreate(cellOptions helpTable[SIZE][SIZE], bruteSum bruteTable[SIZE]);

void bruteSort(bruteSum bruteTable[MAX], int toBrute);

bruteState bruteForceSorted(cell mainTable[SIZE][SIZE],
							cellOptions helpTable[SIZE][SIZE],
							bruteSum bruteTable[MAX],
							int stage,
							int toBrute);