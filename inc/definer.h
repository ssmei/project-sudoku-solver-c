#ifndef DEFINER
#define DEFINER

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX 81  // Max number of cells
#define SIZE 9  // Max number of row / column
#define FILE_IN "input/fillMe.txt"  // File with input sudoku
#define FILE_OUT "output/solved.txt" // File with solved sudoku

typedef enum {BACK, FORWARD} bruteState;  // FORWARD -> moving to the next stage of Brute Force
                                          // BACK    -> .. to previous
typedef enum {NO, YES} flag;
typedef int cell;  // Type of each cell
typedef struct { 
	flag options[10];  // Has the cell an option .options[i]?
	cell brute[9];      // Options for the cell in order from 1 to 9
} cellOptions;  // Name of structure

typedef struct {
	int sum;  // How many options has a cell
	int x;    // cell`s row in mainTable
	int y;    // cell's column in mainTable
} bruteSum;  // Name of structure

#endif
