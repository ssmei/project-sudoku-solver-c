#include "definer.h"

void hello();

flag askSorted();

void printMain(cell mainTable[SIZE][SIZE]);

void printOptions(cellOptions helpTable[SIZE][SIZE], int x, int y);

void helpTableState(cellOptions helpTable[SIZE][SIZE]);

void bruteTableState(bruteSum bruteTable[SIZE], int toBrute);