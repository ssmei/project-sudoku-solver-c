#include "definer.h"

void helpTableCreate(cell mainTable[SIZE][SIZE], cellOptions helpTable[SIZE][SIZE]);

void removeOption(cell mainTable[SIZE][SIZE], 
				  cellOptions helpTable[SIZE][SIZE],
				  int x,
				  int y);

void helpTableRemoveDuplicates(cell mainTable[SIZE][SIZE], 
							   cellOptions helpTable[SIZE][SIZE]);

flag findNewClue(cell mainTable[SIZE][SIZE], cellOptions helpTable[SIZE][SIZE]);