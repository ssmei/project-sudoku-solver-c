#include "gui.h"

void hello() {
// Initial message
	printf("Please insert input sudoku to '");
	printf(FILE_IN); printf("'\n\n");
	printf("You can choose between 2 versions of Brute Force to solve sudoku:\n");
	printf("    Simple -> starts from 1 element in table (1, 1)\n");
	printf("    Sorted -> sort unknown cells based on how many options\n");
    printf("              it has, then starts from less options to the most availible.\n");
}

flag askSorted() {
	char answer;
	while (1) {
		printf("\nWould you like to use SORTED Brute Force? (y/n) ");
		scanf("%c", &answer);
		switch (answer) {
			case 'y': return YES;
			case 'n': return NO;
			default: continue;
		}
	}
	
}

void printMain(cell mainTable[SIZE][SIZE]) {
// show mainTable to stdout

	for (int i=0; i<9; ++i) {
		if (i%3 == 0) printf("------------\n");
		for (int j=0; j<9; ++j) {
			if (j%3==0) printf("|");
			int val;
			if ((val = mainTable[i][j])!=0) printf("%d", val);
			else printf(" ");
		}
		printf("|\n");
	}
	printf("------------\n");
}

void printOptions(cellOptions helpTable[SIZE][SIZE], int x, int y) {
// show options for a cell (x, y)
	
	int option = 0;
	int i=0;
	printf("Options are: ");
	while ((option=helpTable[x][y].brute[i]) != 0) {
		printf("%d ", option);
		++i;
	}
	printf("\n");
}

void helpTableState(cellOptions helpTable[SIZE][SIZE]) { 
// show state of helpTable

	int b=0;
	for (int i=0; i<9; ++i) {
		for (int j=0; j<9; ++j) {
			printf("(%d, %d) ", i, j);
			b=0;
			while (helpTable[i][j].brute[b] != 0) {
				printf("%d ", helpTable[i][j].brute[b++]);
			}
			printf("\n");
		}
	}
}

void bruteTableState(bruteSum bruteTable[MAX], int toBrute) {
// Show state of bruteTable
	
	printf("Printing bruteTable state with %d elements..\n", toBrute+1);
	for (int i=0; i<=toBrute; ++i) {
		printf("%02d. (%d, %d) cell has %02d options.\n", i, bruteTable[i].x, bruteTable[i].y, bruteTable[i].sum);
	}
}