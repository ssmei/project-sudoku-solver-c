#include "helpTable.h"


void helpTableCreate(cell mainTable[SIZE][SIZE], cellOptions helpTable[SIZE][SIZE]) {
// Create helpTable, which has 1 option if cell is a clue, 9 - otherwise

	for (int i=0; i<9; ++i) {
		for (int j=0; j<9; ++j) {
			if (mainTable[i][j]==0) {
				helpTable[i][j] = (cellOptions) { .options = {NO,YES,YES,YES,YES,YES,YES,YES,YES,YES} };
			}
			else {
				helpTable[i][j] = (cellOptions) { .options = {NO,NO,NO,NO,NO,NO,NO,NO,NO,NO} };
				helpTable[i][j].options[mainTable[i][j]] = YES;
			}
		}
	}
}

void removeOption(cell mainTable[SIZE][SIZE], cellOptions helpTable[SIZE][SIZE], int x, int y) { 
// Remove option for a cell if it is already in row or column of box

    int toRemove=mainTable[x][y];
	for (int k=0; k<9; ++k) {
		helpTable[x][k].options[toRemove] = NO;
		helpTable[k][y].options[toRemove] = NO;
	}
	int xBox = x - (x%3); // Calculates start of the box on X
	int yBox = y - (y%3); // Calculates start of the box on Y
	for (int i=0; i<3; ++i) {
		for (int j=0; j<3; ++j) {
			helpTable[xBox+i][yBox+j].options[toRemove] = NO; // 
		}
	}
	//printf("Successfully removed duplicates for (%d, %d).\n", x, y);
	helpTable[x][y].options[toRemove] = YES; // Reassign value to origin cell in helpTable
}


void helpTableRemoveDuplicates(cell mainTable[SIZE][SIZE], cellOptions helpTable[SIZE][SIZE]) { 
// Call removeOption() for each cell

	for (int i=0; i<9; ++i) {
		for (int j=0; j<9; ++j) {
			if (mainTable[i][j]!=0) {
				removeOption(mainTable, helpTable, i, j);
			}
		}
	
	}
}

flag findNewClue(cell mainTable[SIZE][SIZE], cellOptions helpTable[SIZE][SIZE]) { 
// If any cell has only 1 option it is a new clue

	int clue=0;
	int numOptions=0;
	flag newClue=NO;
	for (int i=0; i<9; ++i) {
		for (int j=0; j<9; ++j) {
			clue=0;
			numOptions=0;
			for (int k=1; k<10; ++k) {
				// Checks if there is more than 1 option for a cell in helpTable
				if ((numOptions==1) && (helpTable[i][j].options[k]==YES)) {
					numOptions=2;
					break;
				}
				
				if (helpTable[i][j].options[k]==YES) {
					++numOptions;
					clue=k;
				}
			}
			
			// If new clue found
			if ((clue!=0) && (mainTable[i][j]==0) && (numOptions==1)) {
				newClue = YES;
				mainTable[i][j] = clue;
				removeOption(mainTable, helpTable, i, j);
			}
		}
	}
	
	return newClue;
}
