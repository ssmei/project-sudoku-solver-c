#include "bruteForce.h"

void bruteCreate(cellOptions helpTable[SIZE][SIZE], int x, int y) { 
// Fill .brute field with cell options. .brute[0] -> 1`st element of 'cell' format

	flag changes = NO;
	
	for (int k=1, i=0; k<10; ++k) {
		if (helpTable[x][y].options[k] == YES) {
			changes = YES;
			helpTable[x][y].brute[i++] = k;
		}
	}
	
	if (changes == NO) {
		printf("There is no solution..\n");
	}
}

void helpTableComplete(cellOptions helpTable[SIZE][SIZE]) { 
// Fill .brute field of each cell in helpTable.

	for (int i=0; i<9; ++i) {
		for (int j=0; j<9; ++j) {
			bruteCreate(helpTable, i, j);
		}
	}
}

flag insertMistake(cell mainTable[SIZE][SIZE], int x, int y, int insert) {
// Check if "insert" can be placed in mainTable[x, y] without mistakes
// return YES -> can NOT place it
// return NO -> CAN place it

	for (int k=0; k<9; ++k) {
		if (mainTable[x][k] == insert) {
			return YES;
		}
		if (mainTable[k][y] == insert) {
			return YES;
		}
	}
	
	int xBox = x - (x%3); // Calculates start of the box on X which contains cell mainTable[x][y]
	int yBox = y - (y%3); // Calculates start of the box on Y which contains cell mainTable[x][y]
	
	for (int i=0; i<3; ++i) {
		for (int j=0; j<3; ++j) {
			if (mainTable[xBox+i][yBox+j] == insert) {
				return YES;
			}
		}
	}
	
	return NO;
}

bruteState bruteForce(cell mainTable[SIZE][SIZE], cellOptions helpTable[SIZE][SIZE], int x, int y) {
// Start brute-forcing from from (x, y)

	int bruteIndex=0;
	for (; x<9; ++x) {
		for (; y<9; ++y) {
			// printf("I am here (%d, %d).\n", x, y);
			if (mainTable[x][y]==0) {
				bruteIndex=0;
				while (helpTable[x][y].brute[bruteIndex] != 0) {  // Check if every option for a cell is checked
					flag mistake = insertMistake(mainTable, x, y, helpTable[x][y].brute[bruteIndex]);
					if (mistake==NO) {  // Moving to the next stage
						mainTable[x][y] = helpTable[x][y].brute[bruteIndex];
						if (bruteForce(mainTable, helpTable, x, y+1)==FORWARD) {  // Only Executes if sudoku is solved
							return FORWARD;
						}
					}
					
					++bruteIndex;
				}
				
				mainTable[x][y] = 0;
				return BACK;
			}
			
		}
		
		if (y==9) {  // Solves loop-problem with y
				y=0;
			}
	}
	
	return FORWARD;
}

int bruteTableCreate(cellOptions helpTable[SIZE][SIZE], bruteSum bruteTable[SIZE]) { 
// Creates 1D table of bruteSum which has information only about unknown cells

	int toBrute = 0;
	for (int i=0; i<9; ++i) {
		for (int j=0; j<9; ++j) {
			int k=0;
			if (helpTable[i][j].brute[k+1] != 0) { // If there is more than 1 option for a cell
				bruteTable[toBrute].sum = 0;
				bruteTable[toBrute].x = i;
				bruteTable[toBrute].y = j;
				while (helpTable[i][j].brute[k++] !=0) { // Count options for a cell
					bruteTable[toBrute].sum++;
				}
				++toBrute;
			}
		}
	}
	bruteTable[toBrute].sum = 0;  // It is required to miss the main loop in bruteForceSorted() when stage > toBrute
	--toBrute; // Last is not defined.
	return toBrute; 
}

void bruteSort(bruteSum bruteTable[MAX], int toBrute) {
// Sort elements in bruteTable based on how many options cell has

	int max = toBrute;
	flag change=YES;
	while (change==YES) {
		change = NO;
		for (int i=0; i<max; ++i) {
			if (bruteTable[i].sum > bruteTable[i+1].sum) {
				bruteSum temp = bruteTable[i+1]; // Check if correct
				bruteTable[i+1] = bruteTable[i];
				bruteTable[i] = temp;
				change = YES;
			}
		}
		--max;
	}
}



bruteState bruteForceSorted(cell mainTable[SIZE][SIZE], cellOptions helpTable[SIZE][SIZE], bruteSum bruteTable[MAX], int stage, int toBrute) {
// Start brute-forcing from 'stage'

	int x = bruteTable[stage].x;
	int y = bruteTable[stage].y;
	for (int option=0; option < bruteTable[stage].sum; ++option) {
		flag mistake = insertMistake(mainTable, x, y, helpTable[x][y].brute[option]);
		if (mistake==NO) {
			mainTable[x][y] = helpTable[x][y].brute[option];
			if (bruteForceSorted(mainTable, helpTable, bruteTable, stage+1, toBrute)==FORWARD) {
				return FORWARD;
			}
		}
	}
	
	if (stage > toBrute) {
		return FORWARD;
	}
	
	mainTable[x][y] = 0;
	return BACK;
}
