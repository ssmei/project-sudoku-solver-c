#include <time.h>

#include "dataHandle.h"
#include "gui.h"
#include "helpTable.h"
#include "bruteForce.h"


int main()
{
	cell mainTable[SIZE][SIZE];
	cellOptions helpTable[SIZE][SIZE];
	bruteSum bruteTable[MAX];
	hello();
	flag sorted = askSorted();
	
	clock_t t = clock();  // Start of the timer
	
	read(mainTable);
	helpTableCreate(mainTable, helpTable);
	
	helpTableRemoveDuplicates(mainTable, helpTable);
	
	flag changes=YES;
	while (changes) {
		changes = findNewClue(mainTable, helpTable);
	}
	
	helpTableComplete(helpTable);
	int toBrute=0;
	switch (sorted) {
		case 0:
			if (bruteForce(mainTable, helpTable, 0, 0)==FORWARD) {
				printf("Solved :)\n");
			}
			else {
				printf("No solution :(\n");
			} break;
		
		case 1:
			toBrute = bruteTableCreate(helpTable, bruteTable);  // Number of unknown elements
			bruteSort(bruteTable, toBrute);
			if (bruteForceSorted(mainTable, helpTable, bruteTable, 0, toBrute)==FORWARD) {
				printf("Solved :)\n");
			} 
			else {
				printf("No solution :(\n");
			} break;
			
		default: printf("Unpredictable error..\n"); break;
	}
	
	t = clock() - t;
	double timeTaken = ((double)t)/CLOCKS_PER_SEC;  // End of the timer
	
	printMain(mainTable);
	print(mainTable);

	// helpTableState(helpTable);
	// bruteTableState(bruteTable, toBrute);
	// printMain();
	// print(mainTable);
	
	printf("It took %.3f seconds to solve.\n", timeTaken);
	int a;
	scanf("%d", &a);
}
