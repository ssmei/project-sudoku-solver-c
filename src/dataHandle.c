#include "dataHandle.h"

void read(cell mainTable[SIZE][SIZE]) { 
// Read input sudoku to mainTable from FILE_IN

	FILE* fi = fopen(FILE_IN, "r");
	if (fi != NULL) {
		printf("Opened %s in READ mode.\n", FILE_IN);
		for (int i=0; i<9; ++i) {
			for (int j=0; j<9; ++j) {
				mainTable[i][j] = fgetc(fi) - '0';  // Converts char to int
			}
			fgetc(fi);  // Skip the '\n' character
		}
	}
	else {
        printf("Can NOT open file with input sudoku %s\n", FILE_IN);  // No sudoku table in file.
    }
	fclose(fi);
}

void print(cell mainTable[SIZE][SIZE]) { 
// Write solved sudoku to FILE_OUT

	FILE* fo = fopen(FILE_OUT, "w");
	if (fo != NULL) {
		printf("Opened %s to WRITE solved sudoku.\n", FILE_OUT);
		for (int i=0; i<9; ++i) {
			for (int j=0; j<9; ++j) {
				fprintf(fo, "%d", mainTable[i][j]); // Print each cell to file
			}
			fprintf(fo ,"\n"); // Insert newline in the end of row
		}
	}
	else {
		printf("Can NOT open file to paste final sudoku in %s\n", FILE_OUT);
	}
	fclose(fo);
}
